### What is this repository for? ###

* Transform HTTP Request to model object using a map
* Version v1.0

### How to use ###

## Model
~~~~
class Person
{
    public $name;
    public $age;
}
~~~~

## Request

~~~~
$_POST = array(
            'person_name'   => 'fulano',
            'age_test'      => '21',
        );
~~~~

## Map
#### the property in request 'age_test' not contains on model
~~~~
$mapper = array('age' => ['property' => 'age_test']);
~~~~

## Using
~~~~
RequestMapper::parseToObject($_POST, $mapper, new Person());
~~~~

## Out
~~~~
object(Person)#19 (2) {
  ["name"]=>
  string(6) "fulano"
  ["age"]=>
  string(2) "21"
}
~~~~

### Model prefix ###
## If you use a prefix on the data, like 'person_' the library will map automatic, but if your properties is 'person_tester_name' just use: ##
~~~~
RequestMapper::ParseToObject($_POST, array(), new Person(), 'person_tester');
~~~~

### Default Value ###
## If request dont have a value to object you can set a default value
~~~~
$mapper = array('created_at' => array('default_value' => date("Y-m-d H:i:s")));
~~~~

### Required ###
~~~~
$mapper = array('age' => ['required' => true]);
~~~~