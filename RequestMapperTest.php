<?php

require_once('RequestMapper.php');

class Person
{
    public $name;
    public $age;
}

class RequestMapperTest extends PHPUnit_Framework_TestCase
{
    public function testParseToObjectWorks()
    {
        $source = array(
            'person_name'   => 'fulano',
            'age_test'      => '21',
        );

        $mapper = array('age' => ['property' => 'age_test']);

        /** @var Person $person*/
        $person = RequestMapper::parseToObject($source, $mapper, new Person());

        $this->assertEquals('fulano', $person->name);
        $this->assertEquals('21', $person->age);
    }

    /**
     * @expectedException PropertyRequiredNotFound
     * @expectedExceptionMessage
     */
    public function testShouldThrowExceptionWhenPropertyIsRequired()
    {
        $source = array(
            'person_name'   => 'fulano',
        );

        $mapper = array('age' => ['required' => true]);

        RequestMapper::ParseToObject($source, $mapper, new Person());
    }

    public function testCaseSourceNotHaveValueToPropertySetTheDefaultValueOnMapper()
    {
        $source = array(
            'person_name'   => 'fulano',
        );

        $mapper = array('age' => ['default_value' => 21]);

        $person = RequestMapper::ParseToObject($source, $mapper, new Person());

        $this->assertEquals(21, $person->age);
    }

    public function testPrefixWorks()
    {
        $source = array(
            'person_tester_name'   => 'fulano',
        );

        $person = RequestMapper::ParseToObject($source, array(), new Person(), 'person_tester');

        $this->assertEquals('fulano', $person->name);
    }

    public function testMapperAutomaticWorks()
    {
        $source = array(
            'name'   => 'fulano',
            'age'   => 21,
        );

        $person = RequestMapper::ParseToObject($source, array(), new Person());

        $this->assertEquals('fulano', $person->name);
        $this->assertEquals(21, $person->age);
    }

    public function testHandleWorks()
    {
        $source = array(
            'name'   => 'fulano',
            'age'   => 21,
        );

        $mapper = array('age' => ['handle' => function($value){
                                                return $value + 2;
                                            }]);

        $person = RequestMapper::ParseToObject($source, $mapper, new Person());

        $this->assertEquals(23, $person->age);
    }
}
